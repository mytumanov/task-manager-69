package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

    void add(@NotNull AbstractListener abstractCommand);

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArgument(@NotNull String arg);

    @NotNull
    Collection<AbstractListener> getCommandsWithArgument();

}
