package ru.mtumanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectClearRq;
import ru.mtumanov.tm.dto.response.project.ProjectClearRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-clear";
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final ProjectClearRq request = new ProjectClearRq(getToken());
        @NotNull final ProjectClearRs response = getProjectEndpoint().projectClear(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
