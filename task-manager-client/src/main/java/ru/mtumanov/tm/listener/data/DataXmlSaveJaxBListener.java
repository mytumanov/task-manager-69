package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataXmlSaveJaxbRq;
import ru.mtumanov.tm.dto.response.data.DataXmlSaveJaxbRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataXmlSaveJaxBListener extends AbstractDataListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataXmlSaveJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveJaxbRs response = getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxbRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
