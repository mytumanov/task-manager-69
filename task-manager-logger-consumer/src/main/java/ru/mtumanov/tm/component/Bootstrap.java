package ru.mtumanov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.IReceiverService;
import ru.mtumanov.tm.listener.EntityListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void start() {
        receiverService.receive(entityListener);
    }

}
