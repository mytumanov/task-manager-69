package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.model.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.HashUtil;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @NotNull
    @Transactional
    public User create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.save(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        if (email.isEmpty())
            throw new EmailEmptyException();

        @NotNull final User user = create(login, password);
        user.setEmail(email);
        repository.save(user);
        return user;

    }

    @Override
    @NotNull
    @Transactional
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final User user = create(login, password);
        user.setRole(role);
        repository.save(user);
        return user;

    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        return repository.findByEmail(email);

    }

    @Override
    @NotNull
    public User findById(@NotNull final String id) throws AbstractException {
        if (id.isEmpty())
            throw new EmailEmptyException();
        Optional<User> userOptional = repository.findById(id);
        if (!userOptional.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + id);
        return userOptional.get();
    }

    @Override
    @NotNull
    @Transactional
    public User removeByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        repository.delete(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User removeByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        @NotNull final User user = repository.findByEmail(email);
        repository.delete(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User setPassword(@NotNull final String id, @NotNull final String password) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        Optional<User> userOptional = repository.findById(id);
        if (!userOptional.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + id);
        @NotNull final User user = userOptional.get();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.save(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User userUpdate(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        Optional<User> userOptional = repository.findById(id);
        if (!userOptional.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + id);
        @NotNull final User user = userOptional.get();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.save(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty())
            return false;
        return repository.existsByLogin(login);

    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty())
            return false;
        return repository.existsByEmail(email);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        @NotNull final User user = repository.findByLogin(login);
        user.setLocked(false);
        repository.save(user);
    }

}
