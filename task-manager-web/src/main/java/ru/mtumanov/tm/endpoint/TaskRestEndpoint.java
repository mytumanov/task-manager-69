package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.api.endpoint.ITaskRestEndpoint;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.util.UserUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @NotNull
    @GetMapping
    public Collection<TaskDTO> findAll() {
        List<TaskDTO> projects = taskService.findAllByUserId(UserUtil.getUserId());
        if (projects == null)
            return Collections.emptyList();
        return projects;
    }

    @Override
    @Nullable
    @GetMapping("/{id}")
    public TaskDTO findById(@PathVariable("id") @Nullable final String id) {
        return taskService.findByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @NotNull
    @PostMapping
    public TaskDTO create(@RequestBody @Nullable final TaskDTO task) {
        return taskService.addByUserId(task, UserUtil.getUserId());
    }

    @Override
    @DeleteMapping
    public void delete(@RequestBody @Nullable final TaskDTO task) {
        taskService.removeByUserId(task, UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") @Nullable final String id) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @NotNull
    @PutMapping
    public TaskDTO update(@RequestBody @Nullable final TaskDTO task) {
        return taskService.update(task);
    }
}
