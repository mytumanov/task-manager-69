package ru.mtumanov.tm.service;

import java.util.Collections;

import javax.transaction.Transactional;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.SneakyThrows;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.enumerated.RoleType;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.PermissionException;
import ru.mtumanov.tm.exception.user.RoleEmptyException;
import ru.mtumanov.tm.model.Role;
import ru.mtumanov.tm.model.User;

@Service
public class UserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable RoleType roleType) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (userRepository.existsByLogin(login)) throw new IllegalArgumentException();
        if (roleType == null) throw new RoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role(roleType);
        role.setUser(user);
        user.setRoles(Collections.singletonList(role));
        userRepository.saveAndFlush(user);
    }

    @SneakyThrows
    @Transactional
    public void save(@Nullable final User user) {
        if (user == null) throw new PermissionException();
        userRepository.saveAndFlush(user);
    }

    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @SneakyThrows
    public boolean existsByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.existsByLogin(login);
    }

}
