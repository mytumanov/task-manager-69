package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.SneakyThrows;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.exception.user.AccessDeniedException;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO addByUserId(@Nullable final ProjectDTO project, @Nullable final String userId) {
        if (project == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException();
        project.setUserId(userId);
        return projectRepository.save(project);
    }

    @Override
    @NotNull
    @Transactional
    public ProjectDTO update(@Nullable final ProjectDTO project) {
        if (project == null)
            throw new IllegalArgumentException();
        return projectRepository.save(project);
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final ProjectDTO project) {
        if (project == null)
            throw new IllegalArgumentException();
        projectRepository.delete(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByUserId(@Nullable final ProjectDTO project, @Nullable final String userId) {
        if (project == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException();
        projectRepository.deleteByUserIdAndId(userId, project.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        projectRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    @Transactional
    public ProjectDTO findById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public ProjectDTO findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null)
            throw new IllegalArgumentException();
        if (userId == null || userId.isEmpty())
            throw new AccessDeniedException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

}
