package ru.mtumanov.tm.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.fasterxml.jackson.databind.ObjectMapper;

import ru.mtumanov.tm.model.Message;

public class ServiceAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final AuthenticationException authException
    ) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        final PrintWriter writer = response.getWriter();
        final ObjectMapper mapper = new ObjectMapper();
        final String value = authException.getMessage();

        final Message message = new Message(value);
        writer.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

}
