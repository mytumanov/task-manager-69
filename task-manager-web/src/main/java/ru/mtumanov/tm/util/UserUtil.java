package ru.mtumanov.tm.util;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import lombok.SneakyThrows;
import ru.mtumanov.tm.model.CustomUser;

public interface UserUtil {

    @SneakyThrows
    public static String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
