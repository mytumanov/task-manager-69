package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectService {

    void clear();

    @NotNull
    ProjectDTO addByUserId(@Nullable final ProjectDTO project, @Nullable final String UserId);

    @NotNull
    ProjectDTO update(@Nullable final ProjectDTO project);

    @Nullable
    List<ProjectDTO> findAll();

    @Nullable
    List<ProjectDTO> findAllByUserId(@Nullable final String UserId);

    void remove(@Nullable final ProjectDTO project);

    void removeByUserId(@Nullable final ProjectDTO project, @Nullable final String UserId);

    void removeById(@Nullable final String id);

    void removeByIdAndUserId(@Nullable final String id, @Nullable final String UserId);

    @Nullable
    ProjectDTO findById(@Nullable final String id);

    @Nullable
    ProjectDTO findByIdAndUserId(@Nullable final String id, @Nullable final String UserId);

}
