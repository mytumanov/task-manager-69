package ru.mtumanov.tm.api.repository;

import java.util.List;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectRepository extends JpaRepository<ProjectDTO, String> {

    @Nullable
    List<ProjectDTO> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @Nullable
    ProjectDTO findByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

}
