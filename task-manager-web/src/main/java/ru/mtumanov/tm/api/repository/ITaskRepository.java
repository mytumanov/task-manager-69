package ru.mtumanov.tm.api.repository;

import java.util.List;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.TaskDTO;

@Repository
public interface ITaskRepository extends JpaRepository<TaskDTO, String> {

    void deleteAllByUserId(String userId);

    @Nullable
    List<TaskDTO> findAllByUserId(String userId);

    void deleteByIdAndUserId(String id, String userId);

    @Nullable
    TaskDTO findByIdAndUserId(String id, String userId);

}
