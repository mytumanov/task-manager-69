package ru.mtumanov.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ru.mtumanov.tm.model.Result;
import ru.mtumanov.tm.model.User;

@WebService
@RequestMapping("api/auth")
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/login")
    Result login(@WebParam(name = "username") String username, @WebParam(name = "password") String password);

    @NotNull
    @WebMethod
    @GetMapping("/profile")
    User profile();

    @NotNull
    @WebMethod
    @PostMapping("/logout")
    Result logout();

}
