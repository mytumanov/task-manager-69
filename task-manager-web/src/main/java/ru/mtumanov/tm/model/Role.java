package ru.mtumanov.tm.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.mtumanov.tm.enumerated.RoleType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_role")
public class Role {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "role_type")
    private RoleType roleType = RoleType.USER;

    public Role(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
